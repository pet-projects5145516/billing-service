﻿
using Server;

namespace BillingLogic;

public class Logic
{
    static HashSet<User> _default_users = new HashSet<User>()
                {
                    new User(name:"boris", rating:5000),
                    new User(name:"maria", rating:1000),
                    new User(name:"oleg", rating:800)
                };

    public Logic()
    {
        _Users = _default_users;
        _Coins = new List<Coin>();
    }

    public Logic(HashSet<User> users)
    {
        _Users = users;
        _Coins = new List<Coin>();
    }

    private HashSet<User> _Users;
    private List<Coin> _Coins;


    public List<UserProfile> GetUsersAmount()
    {

        List<UserProfile> dto = new List<UserProfile>();
        foreach (User u in _Users)
            dto.Add(new UserProfile() { Name = u.Name, Amount = u.Coins.Count() });

        return dto;
    }

    public Response CoinsEmission(int n_coins)
    {
        Response response = new Response() { Status = Response.Types.Status.Failed, Comment = "" };


        string comment = "";
        if (n_coins < _Users.Count())
        {
            comment = $"Every user should get a coin. Minimum number of coins to emission is {_Users.Count()}";
        }
        else
        {

            string[] users = (from u in _Users select u.Name).ToArray();
            int[] ratings = (from u in _Users select u.Rating).ToArray();
            int[] given_coins = new int[users.Length];


            int sum_rating = ratings.Sum();
            int coin_left = n_coins;
            int k = 0;

            if (n_coins >= sum_rating)
            {
                k = n_coins / sum_rating;

                for (int i = 0; i < users.Length; i++)
                {
                    given_coins[i] += ratings[i] * k;
                }

                coin_left = n_coins - sum_rating * k;
            }

            if (coin_left != 0)
            {
                double[] justice = new double[users.Length];

                while (coin_left != 0)
                {
                    int i = Array.IndexOf(justice, justice.Min());
                    given_coins[i]++;
                    justice[i] = (double)(given_coins[i] - ratings[i] * k) / ratings[i];
                    coin_left--;
                }
            }

            for (int i = 0; i < users.Length; i++)
                GiveCoins(users[i], given_coins[i]);


            response.Status = Response.Types.Status.Ok;
        }

        response.Comment = comment;
        return response;
    }

    private void GiveCoins(string user_name, int count)
    {
        User? user = (from u in _Users where u.Name == user_name select u).SingleOrDefault();
        if (user != null)
        {
            for (int i = 0; i < count; i++)
            {
                Coin new_coin = new Coin(user);
                user.Coins.Add(new_coin);
                _Coins.Add(new_coin);
            }
        }
    }


    public Response Transfer(string src_user, string dst_user, int amount)
    {
        Response response = new Response() { Status = Response.Types.Status.Failed };
        string comment = "";
        User? src, dst;


        src = _Users.Where(u => u.Name == src_user).SingleOrDefault();
        dst = _Users.Where(u => u.Name == dst_user).SingleOrDefault();

        if (amount <= 0)
        {
            comment = "Amount must be greater than zero";
        }
        else if ((src == null))
        {
            comment = "Source user doesn`t exist!";
        }
        else if ((dst == null))
        {
            comment = "Destination user doesn`t exist!";
        }
        else if (src.Coins.Count() < amount)
        {
            comment = "There are not enough funds in the sender's account";
        }
        else
        {
            List<Coin> tcoins = src.Coins.GetRange(0, amount);

            src.Coins.RemoveRange(0, amount);
            dst.Coins.AddRange(tcoins);

            foreach (Coin c in tcoins) c.Owner = dst;
            response.Status = Response.Types.Status.Ok;
        }



        response.Comment = comment;
        return response;
    }


    public Server.Coin GetLongestHistory()
    {
        Coin max_hist_coin = _Coins.OrderBy(c => c.HistoryOneLine().Length).Last();
        return new Server.Coin() { Id = max_hist_coin.GetHashCode(), History = max_hist_coin.HistoryOneLine() };
    }


}


public class User
{

    public User(string name, int rating)
    {
        Name = name;
        Rating = rating;
        Coins = new List<Coin>();
    }

    public string Name { get; init; }
    public int Rating { get; private set; }

    public List<Coin> Coins;


    public override bool Equals(object? obj)
    {
        if ((obj == null) || !this.GetType().Equals(obj.GetType()))
        {
            return false;
        }
        else
        {
            User u = (User)obj;
            return (u.Name == u.Name);
        }
    }


    public override int GetHashCode() => Name.GetHashCode();


}

public class Coin
{

    public Coin(User owner)
    {
        _History = new List<string>();
        _History.Add("Emission");
        Owner = owner;
    }


    private List<string> _History;

    private User _Owner;

    public User Owner
    {
        get => _Owner;
        set
        {
            _History.Add(value.Name);
            _Owner = value;
        }
    }


    public string HistoryOneLine() => string.Join('-', _History);
}


