using Grpc.Core;
using Server;
using BillingLogic;


namespace Server.Services;

public class BillingService : Billing.BillingBase
{

    private readonly ILogger<BillingService> _logger;

    private readonly Logic logic;

    public BillingService(ILogger<BillingService> logger, Logic l)
    {
        _logger = logger;
        logic = l;
    }

    public override async Task ListUsers(Server.None request, IServerStreamWriter<Server.UserProfile> responseStream, ServerCallContext context)
    {
        List<UserProfile> users = logic.GetUsersAmount();

        foreach (UserProfile user in users)
        {
            await responseStream.WriteAsync(user);
        }
        await Task.CompletedTask;
    }


    public override async Task<Server.Response> CoinsEmission(Server.EmissionAmount request, ServerCallContext context)
    {
        return await Task.FromResult(logic.CoinsEmission((int)request.Amount));
    }


    public override async Task<Server.Response> MoveCoins(Server.MoveCoinsTransaction request, ServerCallContext context)
    {
        return await Task.FromResult(logic.Transfer(request.SrcUser, request.DstUser, (int)request.Amount));
    }


    public override async Task<Server.Coin> LongestHistoryCoin(Server.None request, ServerCallContext context)
    {
        return await Task.FromResult(logic.GetLongestHistory());
    }

}
